package com.kpi;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	// write your code here
        int z = 5208;
        double C3 = z % 3;
        double C17 = z % 17;
        double C30 = (z % 30) + 1;
        System.out.println("C3="+C3+"; C17="+C17+"; C30="+C30);
        //C3=StringBuilder C17=Відсортувати  слова заданого тексту за зростанням кількості голосних літер.
        StringBuilder text = new StringBuilder();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Input text");
        String tempstr;
        do {
            tempstr = scanner.nextLine().trim();
            text.append(tempstr).append(" ");
        } while (tempstr.length() != 0);
        String musor = ".,;:'0123456789+=-*?!()[]{}/|";
        while (getFirstIndex(text, musor) != -1) {
            text.deleteCharAt(getFirstIndex(text, musor));
        }
        tomuchspace(text);
        String[] temp = text.toString().toLowerCase().split(" ");
        text = SortWords(temp);
        System.out.println(text);
    }

    public static int getFirstIndex(StringBuilder GivenText, String tokens) {
        int index = -1;
        for (int i = 0; i < GivenText.length(); i++) {
            for (int j = 0; j < tokens.length(); j++) {
                if (GivenText.charAt(i) == tokens.charAt(j)) {
                    index = i;
                }
            }
        }
        return index;
    }

    public static StringBuilder tomuchspace(StringBuilder GivenText) {
        for (int i = 1; i < GivenText.length(); i++) {
            if (GivenText.charAt(i) == ' ' & GivenText.charAt(i - 1) == ' ') {
                GivenText.deleteCharAt(i);
                i--;
            }
        }
        return GivenText;
    }

    public static int CountLetters(String word) {
        String letters = "аеіоєїяюуыёиaeoiyu";
        int counter = 0;
        for (int i = 0; i < word.length(); i++) {
            for (int j = 0; j < 17; j++) {
                if (word.charAt(i) == letters.charAt(j)) {
                    counter++;
                    break;
                }
            }
        }
        return counter;
    }

    public static StringBuilder SortWords(String[] words) {
        int[] number = new int[words.length];
        for (int i = 0; i < words.length; i++) {
            number[i] = CountLetters(words[i]);
        }
        int temp1;
        String temp2;
        for (int i = 0; i < words.length; i++) {
            int min = i;
            for (int j = i + 1; j < words.length; j++) {
                if (number[j] < number[min]) {
                    min = j;
                }
            }
            temp1 = number[min];
            temp2 = words[min];
            number[min] = number[i];
            words[min] = words[i];
            number[i] = temp1;
            words[i] = temp2;
        }
        StringBuilder Sorted = new StringBuilder();
        for (String word : words) {
            Sorted.append(word).append(" ");
        }
        return Sorted;
    }
}

